# notebook-interpreter
- Java / Spring Boot Notebook Server that can execute pieces of codes of python.

1. [Installation]
    - [Adding variable environment]
    - [Maven Installation]
        - [Maven]
        - [Build and Run project]
2. [Usage]
    - [Api End-Point]
    - [Interpreter request body]
    - [Interpreter response body]
    - [Interpreter response codes]
    
## Maven Installation

### Adding variable environment

Add a variable environment pointing to the folder that will contain user source code file.

Variable name : 

````
UPLOAD_LOCATION
````

### Maven 

install latest version of [Maven](https://maven.apache.org/).


### Build and Run project 

Once maven installed, run a maven clean install package or maven package in the project so the jar file can be generated..

Note : I skipped tests 
````
$ mvn clean install package -U -DskipTests
````

OR 

````
$ mvn package -DskipTests
````

Then You can run the project using the java -jar command.

```
$ java -jar target/interpreter-0.0.1-SNAPSHOT.jar
```

If all is okay, you should be able to interact with the server in:
```
http://localhost:8080/
```

### Build and Run project

You might need to build the project first using Maven.

```
$ mvn package -DskipTests
```

# Usage

### Api End-Point
The Interpreter API is available via http POST method at:
```
/execute
```

### Interpreter request body

The **/execute** interpreter End-Point (POST) accepts JSON as request body. 
The json object must have the following format

```json
{
  "code": "string"
}
```

Here is a small description of the request body fields:
- code: the code to be interpreted, it must have the format:
```
%interpreter code
```
supported language : Python

- sessionId: is the id of the session we are using. this field is used to differentiate between users and also to allow
users preserve their code state.


### Interpreter response body
```json
{
  "result": "string"
}
```
### Interpreter response codes

- **200** SUCCESS: The interpreter API returns an HTTP SUCCESS response code in case of success. The response will have the format 
described above and might containing errors details.

- **400** BAD_REQUEST: The APi might return BAD REQUEST as response code in the following cases :
    - Invalid Interpret Request: this error occur in the following cases :
        - The field __"code"__ is empty or null.
        - The field __"code"__ doesnt follow the format %interpreter code
        
    - Invalid Request Format: In case the request doesnt follow the correct format (similar to Invalid Interpret Request)
    
    - Language Not Supported: The language specified in the request is not supported by the API
    
    - Execution request taking too long: timeout (More than 1 second).
   
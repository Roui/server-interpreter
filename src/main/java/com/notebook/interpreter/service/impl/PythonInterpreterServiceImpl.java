package com.notebook.interpreter.service.impl;

import com.notebook.interpreter.model.Interpreter;
import com.notebook.interpreter.service.InterpreterServiceImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class PythonInterpreterServiceImpl extends InterpreterServiceImpl {
    /**
     * {@inheritDoc}
     */
    @Override
    public Interpreter getInterpreterLanguage() {
        return Interpreter.PYTHON;
    }
}

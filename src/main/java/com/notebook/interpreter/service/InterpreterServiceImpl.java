package com.notebook.interpreter.service;

import com.notebook.interpreter.configuration.ApplicationProperties;
import com.notebook.interpreter.model.ExecutionContext;
import com.notebook.interpreter.model.ExecutionRequest;
import com.notebook.interpreter.model.ExecutionResponse;
import com.notebook.interpreter.model.Interpreter;
import com.notebook.interpreter.model.exception.LanguageNotSupportedException;
import com.notebook.interpreter.model.exception.TimeOutException;
import com.notebook.interpreter.utils.parsers.CodeType;
import com.notebook.interpreter.utils.parsers.SourceCodeParserFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public abstract class InterpreterServiceImpl implements InterpreterService {
    @Autowired
    private ApplicationProperties applicationProperties;
    private static final int COMMAND_INDEX = 0;
    private static final int COMMAND_FIRST_ARG_INDEX = 1;

    /**
     * {@inheritDoc}
     */
    public ExecutionResponse execute(final ExecutionRequest request) {
        // Check if language is supported
        if (unsupportedLanguage()) { throw new LanguageNotSupportedException(); }

        ExecutionResponse executionResponse = new ExecutionResponse();
        ExecutionContext executionContext = new ExecutionContext(request);

        try {
            saveOrUpdateSourceCodeFile(executionContext);
            Process process = executeCommand(executionContext.getInterpreter().name(), executionContext.getSourceCodeFilePathname());
            String output;
            output = new String(process.getInputStream().readAllBytes());
            String errors;
            errors = new String(process.getErrorStream().readAllBytes());
            executionResponse.setOutput(output);
            executionResponse.setErrors(errors);
        } catch (IOException | InterruptedException e) {
            Thread.currentThread().interrupt();
            return new ExecutionResponse("", e.getMessage());
        }
        return executionResponse;
    }

    private Process executeCommand(final String... commandParts) throws IOException, InterruptedException {
        Process process = new ProcessBuilder(commandParts[COMMAND_INDEX], commandParts[COMMAND_FIRST_ARG_INDEX]).start();
        long timeout = applicationProperties.getTimeOutDuration();

        if(!process.waitFor(timeout, TimeUnit.MILLISECONDS)) {
            process.destroyForcibly();
            throw new TimeOutException();
        }

        return process;
    }

    private boolean unsupportedLanguage() {
        String interpreterLanguage = getInterpreterLanguage().getName();

        for (Interpreter i : Interpreter.values()) {
            if (i.name().equals(interpreterLanguage)) {
                return true;
            }
        }

        return false;
    }


    private void saveOrUpdateSourceCodeFile(final ExecutionContext executionContext) throws IOException {
        String codeSourceFilePathName = executionContext.getSourceCodeFilePathname();
        File f = new File(codeSourceFilePathName);

        if(f.exists() && !f.isDirectory()) {
            SourceCodeParserFactory.makeSourceCodeParser(executionContext.getInterpreter()).parseSourceCodeFile(codeSourceFilePathName, CodeType.GLOBAL_VARIABLE);
            Files.write(Paths.get(codeSourceFilePathName), executionContext.getExecutionRequest().getCode().getBytes(), StandardOpenOption.APPEND);
        } else {
            Files.write(Paths.get(codeSourceFilePathName), executionContext.getExecutionRequest().getCode().getBytes());
        }
    }
}

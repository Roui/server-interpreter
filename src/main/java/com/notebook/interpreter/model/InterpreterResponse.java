package com.notebook.interpreter.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InterpreterResponse {
    @JsonProperty("result")
    private String response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}

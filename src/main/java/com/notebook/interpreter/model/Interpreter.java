package com.notebook.interpreter.model;

public enum Interpreter {
	PYTHON("python"),
	C("C");

	private String name;
	Interpreter(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static Interpreter getInterpreterFromLanguageName(String language) {
		for (Interpreter interpreter : Interpreter.values()) {
			if (interpreter.name.equalsIgnoreCase(language)) {
				return interpreter;
			}
		}
		return null;
	}

	public static String getInterpretedFileExtensionFor(Interpreter interpreter) {
		switch (interpreter) {
			case PYTHON:
				return ".py";
			case C:
				return "c";
			default:
				return "";
		}
	}
}

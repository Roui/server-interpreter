package com.notebook.interpreter.model;

public class ExecutionContext {
    private ExecutionRequest executionRequest;

    public ExecutionContext(ExecutionRequest executionRequest) {
        this.executionRequest = executionRequest;
    }

    public String getSourceCodeFilePathname() {
        return System.getenv("UPLOAD_LOCATION") + "\\" + executionRequest.getSessionId() + Interpreter.getInterpretedFileExtensionFor(getInterpreter());
    }

    public Interpreter getInterpreter() {
        return Interpreter.getInterpreterFromLanguageName(executionRequest.getLanguage());
    }

    public ExecutionRequest getExecutionRequest(){
        return executionRequest;
    }
}

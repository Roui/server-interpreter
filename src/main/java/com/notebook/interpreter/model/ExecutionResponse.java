package com.notebook.interpreter.model;

public class ExecutionResponse {
    String output;
    String errors;

    public ExecutionResponse() {
        // no args constructor
        output = "";
        errors = "";
    }

    public ExecutionResponse(String output, String errors) {
        setOutput(output);
        setErrors(errors);
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output.replaceAll("\\s|\\\\r|\\\\n", " ");
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = removeWhiteSpaces(replaceUriInString(errors));
    }

    private String removeWhiteSpaces(String string) {
        return string.replaceAll("\\s|\\\\r|\\\\n", " ");
    }

    private String replaceUriInString(String string){
     return string.replaceAll(".*File \".*\\.py\".*", " ");
    }
}

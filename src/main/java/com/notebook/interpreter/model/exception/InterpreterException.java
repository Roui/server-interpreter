package com.notebook.interpreter.model.exception;

public class InterpreterException extends RuntimeException {
    public InterpreterException(){}
    public InterpreterException(String message){super(message);}
}

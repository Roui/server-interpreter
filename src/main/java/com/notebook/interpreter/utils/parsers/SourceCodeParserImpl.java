package com.notebook.interpreter.utils.parsers;

import com.notebook.interpreter.model.Interpreter;
import com.notebook.interpreter.model.exception.InvalidInterpreterRequestException;
import com.notebook.interpreter.utils.parsers.SourceCodeParser;

public abstract class SourceCodeParserImpl implements SourceCodeParser {
    public void checkFileNameValidity(String filePathname){
        if(filePathname.matches("^.*\\." + Interpreter.getInterpretedFileExtensionFor(getInterpreter()) + "$"))
            throw new InvalidInterpreterRequestException();
    }
}

package com.notebook.interpreter.utils.parsers;

import com.notebook.interpreter.model.Interpreter;

import java.io.IOException;

public interface SourceCodeParser {
    void parseSourceCodeFile(String filePathname, CodeType... parsedCodeTypes) throws IOException;
    Interpreter getInterpreter();
}

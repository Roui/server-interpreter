package com.notebook.interpreter.utils.parsers.impl;

import com.notebook.interpreter.model.Interpreter;
import com.notebook.interpreter.model.exception.InvalidInterpreterRequestException;
import com.notebook.interpreter.utils.parsers.CodeType;
import com.notebook.interpreter.utils.parsers.SourceCodeParserImpl;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * parses a source code file based on code type
 *
 */
public class PythonSourceCodeParserImpl extends SourceCodeParserImpl {
    private static final String GLOBAL_VARIABLES_ASSIGNMENT_PATTERN = "^.*=.*$";

    @Override
    public void parseSourceCodeFile(final String filePathname, final CodeType... parsedCodeTypes) throws IOException {
        checkFileNameValidity(filePathname);
        List<String> newLines = new ArrayList<>();
        for(CodeType codeType : parsedCodeTypes) {
            switch (codeType) {
                case GLOBAL_VARIABLE:
                    newLines.addAll(parseGlobalVariables(filePathname));
                    break;
                //other cases
                default:
                    break;
            }
        }
        Files.write(Paths.get(filePathname), newLines, StandardCharsets.UTF_8);
    }

    private List<String> parseGlobalVariables(final String filePathname) throws IOException {
        checkFileNameValidity(filePathname);
        List<String> newLines = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(filePathname));
        try {
            String st;
            while ((st = br.readLine()) != null) {
                if (st.matches(GLOBAL_VARIABLES_ASSIGNMENT_PATTERN)) {
                    newLines.add(st);
                }
            }
        } finally {
            br.close();
        }
        return newLines;
    }

    public Interpreter getInterpreter() {
        return Interpreter.PYTHON;
    }
}


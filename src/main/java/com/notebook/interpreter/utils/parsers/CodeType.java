package com.notebook.interpreter.utils.parsers;

public enum CodeType {
    GLOBAL_VARIABLE,
    METHOD,
}

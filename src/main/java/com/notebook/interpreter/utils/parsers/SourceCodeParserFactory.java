package com.notebook.interpreter.utils.parsers;

import com.notebook.interpreter.model.Interpreter;
import com.notebook.interpreter.model.exception.InterpreterException;
import com.notebook.interpreter.utils.parsers.impl.PythonSourceCodeParserImpl;

public class SourceCodeParserFactory {
    private SourceCodeParserFactory() {throw new IllegalStateException();}

    public static SourceCodeParser makeSourceCodeParser(Interpreter interpreter) {
        switch (interpreter) {
            case PYTHON:
                return new PythonSourceCodeParserImpl();

            default:
                throw new InterpreterException();
        }
    }
}

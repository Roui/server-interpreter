package com.notebook.interpreter.controller;

import com.notebook.interpreter.model.*;
import com.notebook.interpreter.service.*;
import com.notebook.interpreter.validation.CorrectRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@Validated
public class InterpreterController {

    @Autowired
    private InterpreterRequestParsingService interpreterRequestParsingService;

    @Autowired
    private InterpreterServiceFactory interpreterServiceFactory;

    @PostMapping("/execute")
    public InterpreterResponse execute(@CorrectRequest @RequestBody InterpreterRequest interpreterRequest, HttpSession httpSession) {
        ExecutionRequest request = interpreterRequestParsingService.parseInterpreterRequest(interpreterRequest);
        InterpreterService interpreterService = interpreterServiceFactory.getInterpreterService(request.getLanguage());
        String sessionId = interpreterRequest.getSessionId() != null ? interpreterRequest.getSessionId() : httpSession.getId();
        request.setSessionId(sessionId);
        ExecutionResponse executionResponse = interpreterService.execute(request);
        InterpreterResponse interpreterResponse = new InterpreterResponse();
        String response = executionResponse.getErrors().equals("") ? executionResponse.getOutput() : executionResponse.getErrors();
        interpreterResponse.setResponse(response);
        return interpreterResponse;
    }
}
